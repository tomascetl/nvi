= Move semantics: smart pointers
:source-highlighter: highlightjs
:revealjs_theme: night

== Brief recap

[%step]
* _r-value references_
+
[source,cpp]
----
int foo(T &&);
----

* pass the value using `*std::move*`
+
[source,cpp]
----
foo(std::move(variable));
----

* however, prefer receiving  by value instead of by _r-value reference_ (unless it is move ctor)
+
[source,cpp]
----
int foo(T);
// caller may choose how to pass the value
foo(IwannaKeepThis);
foo(std::move(IdoNotNeedThis));
----


== Topics

[%step]
* smart pointers
** `*std::unique_ptr*`
** `*std::shared_ptr*`
** `*std::weak_ptr*`
* inheritance
* _NVI_ idiom
* _visitor_ pattern

== `std::unique_ptr`

[%step]
* a simple RAII object for memory allocation
* usefull for objects with interface
* create via `*std::make_unique*`
* supports move only (no copy)
* owner holds the data
* pass oldschool raw pointers to processing
* *DO NOT USE IN PARALLEL ENVIRONMENT*

== `std::unique_ptr`

[source,cpp]
----
// prefer not to use plain `new'
auto ptr = std::make_unique<Object>("arg", "arg");
process(ptr.get()); // pass a raw pointer

ptr->someMethod();
ptr.reset();
if (ptr)
    std::cout << "this will never print";

----

== `std::shared_ptr`

[%step]
* a quite interesting RAII object for memory allocation
* use reference counting
* a wrong usage may lead to memory leaks
** `*std::weak_ptr*` is usefull
* supports both copy and move
* use mainly in parallel environment
* *ONLY PARTIALLY THREAD SAFE*

== `std::weak_ptr`

[%step]
* its relation to `*std::shared_ptr*` is similar to 
  relation of raw pointer to `*std::unique_ptr*`
* temporarily cast to `*std::shared_ptr*` if you need
  to work with the pointed object:
+
[source,cpp]
----
std::shared_ptr object = std::make_shared<Object>();
std::weak_ptr ref = object;
if (auto ptr = ref.lock())
    ptr->doSomething();
----

== Inheritance

The class can drive its inheritance properties:

[%step]
* forbid inheritance
+
[source,cpp]
----
struct Foo final : Bar {};
----

* force inheritance, cannot be accessed through
+
[source,cpp]
----
struct Foo {
protected:
    ~Foo() = default;
};
----

* be inheritance-friendly
+
[source,cpp]
----
struct Foo {
    virtual ~Foo() = default;
};
----

== Inheritance

Our socket approach

[source,cpp]
----
struct Socket : protected FileDescriptor {
    using FileDescriptor::read;
    using FileDescriptor::write;
    // more of them...
};
----

[%step]
* forbids casting to the base class
* allow using method from the base class
* super annoying

== Inheritance: thoughts

[%step]
* Classes expected to be dynamically allocated definitelly must handle this issue.
* Classes used in hierarchy should handle this issue.
* RAII classes do not need to address this issue.
* Classes used on the stack do not need to address this issue.
* *virtual* destructor may give wrong sign.
** misleading in template-like coding

== NVI idiom

[%step]
* Make public methods (interface) non-virtual.
* Make virtual methods *private*.
[%step]
** Make them *protected* if they need to be called from a child.

== NVI idiom

**N**on-**V**irtual **I**nterface idiom

[source,cpp]
----
struct Interface {
    virtual ~Interface() = default;
    bool something() {
        if (please())
            return doSomething();
        return false;
    }
private:
    virtual bool please() = 0;
    virtual bool doSomething() = 0;
};
----

== NVI idiom

Reasons why:

[%step]
* pros
[%step]
** separate what you provide vs. how it is achieved
** add common logs/checks/... in the interface methods
* cons
[%step]
** slightly longer code

== Double dispatch

.Fight bonus/malus
|===
|Attacker-> |Hobbit |Elf |Dwarf

|Hobbit
|0
|+1
|+3

|Elf
|-2
|0
|-1

|Dwarf
|-4
|+1
|0
|===

== Double dispatch

How to implement the table using virtual methods?

[%step]
* The dispatch needs to be done on both parameters.
* In most modern languages, the virtual dispatch can be done on the first argument only.
* A ping-pong dispatch needs to be done.

== Double dispatch

[source,cpp]
----
struct Hobbit; struct Elf; struct Dwarf;
struct Race {
    virtual int attack(const Race &) const = 0;
    virtual int hurtBy(const Hobbit &) const = 0;
    virtual int hurtBy(const Elf &) const = 0;
    virtual int hurtBy(const Dwarf &) const = 0;
};
struct Hobbit : Race {
    int attack(const Race &target) const override {
        return target.hurtBy(*this);
    }
    int hurtBy(const Hobbit &) const override { return 0; }
    int hurtBy(const Elf &) const override { return 1; }
    int hurtBy(const Dwarf &) const override { return 3; }
};
----

== Visitor pattern

[%step]
* built on the top of the double dispatch
* split the `*Race*` class
[%step]
** visitor implements the `*hurtBy*` methods
** visitees implement the `*attack*` method

== Visitor pattern

[source,cpp]
----
struct TextNode; struct ArrayNode; struct DictNode;
struct Visitor {
    virtual void process(TextNode &) = 0;
    virtual void process(ArrayNode &) = 0;
    virtual void process(DictNode &) = 0;
};
struct Node {
    virtual void accept(Visitor &) = 0;
};
struct TextNode : Node {
    void accept(Visitor &v) override { v.process(*this); }
};
struct Export : Visitor {
    void process(TextNode &t) override { t.foo(); }
};
----

== Let's do the task!

* form new pairs
* reach README.ad
* ask if you get stuck
* https://cppreference.com
** unique_ptr
** map
** vector
