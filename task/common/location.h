#ifndef LOCATION_H
#define LOCATION_H

#include <iostream>

#if defined __has_include && __has_include(<source_location>)
# include <source_location>
namespace source {
using Location = std::source_location;
}
#elif defined __has_include && __has_include(<experimental/source_location>)
# include <experimental/source_location>
namespace source {
using Location = std::experimental::source_location;
}
#else

namespace source {
struct Location {
    constexpr Location() noexcept 
        : _function("unknown")
        , _file(_function)
        , _line(0)
    {}

    static constexpr Location current(
        const char *function = __builtin_FUNCTION(),
        const char *file = __builtin_FILE(),
        unsigned line = __builtin_LINE()) noexcept
    {
        return { file, function, line };
    }

    constexpr const char *function_name() const noexcept { return _function; }
    constexpr const char *file_name() const noexcept { return _file; }
    constexpr unsigned line() const noexcept { return _line; }
    constexpr unsigned column() const noexcept { return 0; }

private:
    Location(const char *function, const char *file, unsigned line) noexcept
        : _function(function)
        , _file(file)
        , _line(line)
    {}

    const char *_function;
    const char *_file;
    unsigned _line;
};

} // namespace source

#endif

#endif
