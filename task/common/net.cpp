#include "net.h"

#include <algorithm>
#include <cstring>
#include <iomanip>
#include <sys/signalfd.h>
#include <signal.h>

#include "system-error.h"

namespace net {

namespace {

int parseInt(std::string_view port) {
    log::here() << std::quoted(port);
    char *end;
    int result = std::strtol(&port.front(), &end, 10);
    if (end != port.end() || errno == ERANGE)
        throw std::runtime_error("port is not a number");
    return result;
}

std::vector<int> parseInts(std::string_view ports) {
    log::here() << std::quoted(ports);

    std::vector<int> result;
    while (!ports.empty()) {
        auto pos = ports.find(',');
        auto port = ports.substr(0, pos);
        ports.remove_prefix(port.size());
        if (!ports.empty() && ports.front() == ',')
            ports.remove_prefix(1);

        result.push_back(parseInt(port));
    }
    return result;
}

} // namespace <Anonymous>

Server::Server(std::string_view port)
    : Server(parseInts(port))
{}

Server::Server(std::vector<int> ports) {
    for (int port : ports) {
        common::Socket ear;
        ear.bind(common::Address("0.0.0.0", port));
        auto address = ear.getAddress();

        here() << address.host() << ":" << address.port().value_or(-1);
        _ears.emplace_back(std::move(address), std::move(ear));
    }
}

void Server::setupSignals() {
    sigset_t mask;
    struct signalfd_siginfo fdsi;

    sigemptyset(&mask);
    sigaddset(&mask, SIGINT);
    sigaddset(&mask, SIGQUIT);
    sigaddset(&mask, SIGTERM);

    if (0 > ::sigprocmask(SIG_BLOCK, &mask, nullptr))
        throw common::SystemError("sigprocmask");

    _signalListener = common::FileDescriptor(::signalfd(common::FileDescriptor::Invalid, &mask, 0));
    if (!_signalListener)
        throw common::SystemError("signalfd");
}

void Server::startListening() {
    here();
    for (auto &ear : _ears)
        ear.listen(32);
}

auto Server::split(const std::vector<pollfd> &fds, const std::map<int, SocketReference> &mapping) {

    bool quit = fds.front().revents & POLLIN;
    std::vector<Socket *> readyToAccept;
    std::vector<SocketReference> readyToReceive;
    std::vector<SocketReference> readyToClose;

    if (!quit) {

        auto ears = fds.begin() + 1;
        auto clients = ears + _ears.size();

        auto ready = _ears.begin();
        for (auto &fd : common::Range(ears, clients)) {
            if (fd.revents & POLLIN)
                readyToAccept.push_back(&*ready);
            ++ready;
        }

        for (auto &fd : common::Range(clients, fds.end())) {
            if (!fd.revents)
                continue;

            auto m = mapping.find(fd.fd);
            if (m == mapping.end())
                throw std::logic_error("should not happen");

            if (fd.revents & POLLNVAL)
                readyToClose.push_back(m->second);
            else if (fd.revents & POLLIN)
                readyToReceive.push_back(m->second);
        }
    }

    return std::tuple(
        quit,
        std::move(readyToAccept),
        std::move(readyToReceive),
        std::move(readyToClose)
    );
}

auto Server::preparePollFds() {
    std::vector<pollfd> fds;
    fds.reserve(1 + _ears.size() + _clients.size());

    fds.push_back(_signalListener.toPoll());
    for (auto &ear : _ears)
        fds.push_back(ear.toPoll());

    auto mapping = _clients.toPoll(fds);
    return std::pair(std::move(fds), std::move(mapping));
}

void Server::run(EventHandler &handler) {
    setupSignals();
    startListening();

    while (true) {
        auto [ fds, mapping ] = preparePollFds();
        if (0 > ::poll(fds.data(), fds.size(), -1))
            throw common::SystemError("poll");

        auto [ quit, readyToAccept, readyToReceive, readyToClose ] = split(fds, mapping);

        if (quit)
            break;

        for (auto *ear : readyToAccept) {
            handler.accept(_clients.add(ear->accept()));
        }

        for (auto &s : readyToReceive) {
            if (!handler.receive(s.id(), *_clients.find(s.id())))
                _clients.remove(s.id());
        }

        for (auto &s : readyToClose) {
            _clients.remove(s.id());
            handler.close(s.id());
        }
    }
    here() << "quit...";
}

void Server::accept(const std::vector<Socket *> &ready, EventHandler &handler) {
}

} // namespace net
