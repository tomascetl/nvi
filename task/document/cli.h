#ifndef DOC_CLI_H
#define DOC_CLI_H

#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>

#include "command.h"
#include "document.h"
#include "error.h"
#include "../common/logger.h"

namespace doc {

struct CLI : log::Enable<CLI> {

    static constexpr const std::string_view prompt = "!> ";
    static constexpr const std::string_view welcome = 
        "Document builder CLI. Close standard input to exit (^D in bash).\n" 
        "Type `help' to see available parameters.\n";
    /**
     * @brief Start CLI in the interactive mode
     * 
     */
    void run() {
        here() << "start";
        Document document;

        std::string line;
        std::cout << welcome << std::flush;
        while (std::cin.good()) {
            std::cout << prompt << std::flush;
            std::getline(std::cin, line);
            if (std::cin.eof())
                break;
            std::cout << execute(document, line) << std::flush;
        }
        here() << "finish";
    }

    /**
     * @brief Execute one command on the document.
     * 
     * @param document 
     * @param line 
     * @return std::string 
     */
    std::string execute(Document &document, std::string &line) try {
        auto [ cmd, params ] = parse(std::istringstream{line});
        return process(document, cmd, std::move(params));
    } catch (const Error &e) {
        using namespace std::literals;
        return "Error: "s + e.what() + '\n';
    }

private:
    struct Help : Command {
        static constexpr std::string_view cmdName = "help";
        Help(CLI &cli)
            : _cli(cli)
        {}

    private:
        std::optional<std::string> _process(Document &, Params params) override {
            return _cli.generateHelp(std::move(params));
        }

        std::string _help() const override {
            return "Provides help for other commands.";
        }

        std::string_view _name() const override {
            return cmdName;
        }

        Range _arguments() const override {
            return {0, std::nullopt};
        }

        CLI &_cli;
    };

    std::string process(Document &document, std::string_view cmd, Params params) {
        auto c = _commands.find(cmd);
        if (c != _commands.end())
            return c->second->process(document, std::move(params));

        if (!cmd.empty())
            return invalidCommand(cmd);
        return {};
    }

    static std::pair<std::string, Params> parse(std::istringstream line) {
        std::string cmd;
        Params params;
        std::string arg;

        line >> cmd;

        while (line >> quoted(arg))
            params.emplace_back(arg);
        return {cmd, params};
    }

    std::string generateHelp(Params params) const {
        std::ostringstream out;
        if (params.empty()) {
            generateCommands(out);
        }
        else {
            for (auto &name : params) {
                auto c = _commands.find(name);
                if (c == _commands.end())
                    out << "\nunknown command " << quoted(name) << '\n';
                else
                    out << '\n' << c->second->help() << '\n';
            }
        }
        return out.str();
    }

    std::string invalidCommand(std::string_view cmd) const {
        std::ostringstream out;
        out << "Unknown command " << quoted(cmd) << "!\n";
        generateCommands(out);
        return out.str();
    }

    std::ostream &generateCommands(std::ostream &out) const {
        out << "List of available commands:\n";
        for (auto &[name,_]  : _commands) {
            out << '\t' << name << "\n";
        }
        return out;
    }

    const std::map<std::string_view, std::unique_ptr<Command>> _commands{[&]{
        std::map<std::string_view, std::unique_ptr<Command>> map;
        map.insert(makeCmd<Help>(*this));
        map.insert(makeCmd<Add>());
        map.insert(makeCmd<Export>());
        map.insert(makeCmd<Encode>());
        map.insert(makeCmd<Status>());
        map.insert(makeCmd<Drop>());
        return map;
    }()};
};

} // namespace doc

#endif
